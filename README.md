# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# Easy Paper Backups of Ascii PGP Keys

Shell scripts to convert between ascii armor PGP keys and QR codes for paper backup.

After exporting your private keys in ascii armor format, you can use the scripts
in this project to convert them to PNG images that can be printed and archived.

## Dependencies

This project depends on a couple libraries that come with applicatons that are called
by these scripts. You can download the files to Jenkins\project_worrk_spec folder

1. [libqrencode](http://fukuchi.org/works/qrencode/)
2. [zbar](http://zbar.sourceforge.net)

## Export keys From GPG

There are several good guides on the web about managing GPG keys and creating backups.
The quick version is to use one of these commands:

    gpg --armor --export > pgp-public-keys.asc
    gpg --armor --export-secret-keys > pgp-private-keys.asc
    gpg --armor --gen-revoke [your key ID] > pgp-revocation.asc

**NOTE** Be sure to securely remove your private and revocation keys once they
are correctly backed up. This can be done from the command line with either the 'srm'
tool on Max OS X or with 'shred' on Linux.

## Convert To QR Code Images

Use the asc2qr.sh script to convert a public or private key in ascii armor format
into QR code PNG images.

Print the resulting images and save them in a fireproof safe or safety deposit box.

## Convert To Ascii Armor Key Files

Use the qr2asc.sh script to convert QR code images (created from an photo of
the image) to a public or private key in ascii armor format.

    
## Import Keys Into GPG

To import keys into GPG use one of these commands:

    gpg --import pgp-public-keys.asc
    gpg --import pgp-private-keys.asc
